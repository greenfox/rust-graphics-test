# Rust Graphics Test



## Install Rust

Go here and follow instructions: https://rustup.rs/. You'll have to restart your
terminal for changes to take effect.

## Create crate

Run `cargo new $Proj_Name`(replace $Proj_Name with your project's name). This will create a new directory for your project as well as `git init`.

## Add Dependencies

You now have a file `Cargo.toml`, add this to your `[dependencies]`:

```toml
[dependencies]
glow = "0.9.0"
glutin = "0.26"
```

## Add Source

Replace `src/main.rs` with the one from this repo.

We'll go through what this does, line by line.

## Build/Run

- `cargo run`
- `cargo build`
- `cargo build --release`

### Adding other build targets

#### Windows on Linux

- Run: `rustup target add x86_64-pc-windows-gnu`
- Install Migwin: `sudo apt install mingw-w64`
- Add linker to `Cargo.toml`:

```
[target.x86_64-pc-windows-gnu]
linker = "/usr/bin/x86_64-w64-mingw32-gcc"
```

`cargo build --target x86_64-pc-windows-gnu --release`

#### Linux on Windows

TODO!

# VSCode Intigrations

## VSCode Plugins:

- `swellaby.rust-pack`, includes: `rust-lang.rust`, `serayuzgur.crates`, `bungcip.better-toml`
- `vadimcn.vscode-lldb`

## Setup Build and Debugging

Apperantly, with just these installed, F5 will complain, but then it will just work. Needs more testing.
