use cgmath::*;
// use cgmath::Transform;

pub struct Camera {
    pub rotation: Quaternion<f32>,
    pub translation: Vector3<f32>,
    pub shape: Matrix4<f32>,
}

impl Camera {
    #[allow(dead_code)]
    pub fn new() -> Self {
        let rotation = Quaternion::new(0.0, 0.0, 0.0, 1.0);
        let translation = Vector3::new(0.0, 0.0, 0.0);
        Camera {
            rotation,
            translation,
            shape: Matrix4::identity(),
        }
    }
    pub fn perspective(cam: &Camera, angle: f32, aspect: f32, near: f32, far: f32) -> Self {
        let rotation = cam.rotation; //Quaternion::new(0.0,0.0,0.0,1.0);
        let translation = cam.translation; // Vector3::new(0.0,0.0,0.0);
        let shape = cgmath::perspective(cgmath::Deg(angle), aspect, near, far);
        Camera {
            rotation,
            translation,
            shape,
        }
    }

    pub fn to_mat(&self) -> Matrix4<f32> {
        self.shape * Matrix4::from(self.rotation) * Matrix4::from_translation(-self.translation)
    }
}
