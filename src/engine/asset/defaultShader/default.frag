#version 300 es
//frag
// #extension GL_ARB_separate_shader_objects : enable

precision highp float;


out vec4 FragColor;

in vec3 normalShift;
in vec2 UV;


void main() {
   FragColor = vec4(UV,0.0, 1.0f);
}
