#version 300 es 
//vert
// #extension GL_ARB_separate_shader_objects : enable

precision highp float;

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 uv;

uniform mat4 ModelViewProjection;

out vec3 normalShift;
out vec2 UV;

void main() {
    gl_Position = ModelViewProjection * vec4(vertex, 1.0);
    normalShift = (normal + 1.0)/2.0;
    UV = uv;
}
