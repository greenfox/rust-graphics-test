mod model;
pub use model::*;

mod shader;
pub use shader::*;

mod transform;
pub use transform::*;

mod camera;
pub use camera::*;

mod physics;
pub use physics::*;

trait Draw {
    fn draw(&self) {
        assert!(false, "draw not implemented")
    }
}

trait Asset {}
