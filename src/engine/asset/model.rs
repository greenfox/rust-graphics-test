use glow::*;

// use gl;
#[allow(dead_code)]
pub struct Model {
    vbo: glow::Buffer,
    vao: glow::VertexArray,
    // #[allow(dead_code)]
    verts: Vec<f32>,
    size: i32,
    mode: u32,
}

fn check_error(gl: &glow::Context) {
    let a = unsafe { gl.get_error() };
    if a != 0 {
        println!("gl error:{}", a);
    }
}

impl Model {
    // #[allow(dead_code)]
    // pub fn new()->Model
    // {
    //     Model{vbo:0,vao:0,verts:vec![],mode:0,size:0}
    // }
    #[allow(dead_code)]
    pub fn from_verts(verts: Vec<f32>, gl: &glow::Context, mode: u32) -> Model {
        unsafe {
            // set up vertex data (and buffer(s)) and configure vertex attributes
            // ------------------------------------------------------------------
            // HINT: type annotation is crucial since default for float literals is f64

            // let (mut vbo, mut vao) = (0, 0);
            let vao = gl.create_vertex_array().unwrap(); //::GenVertexArrays(1, &mut vao);
            check_error(gl);
            gl.bind_vertex_array(Some(vao)); //gl::BindVertexArray(vao);

            let vbo = gl.create_buffer().unwrap(); //gl::GenBuffers(1, &mut vbo);
            check_error(gl);
            // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
            gl.bind_buffer(glow::ARRAY_BUFFER, Option::from(vbo)); //gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
            check_error(gl);

            // gl.buffer_data_size(target: u32, size: i32, usage: u32)
            gl.buffer_data_size(glow::ARRAY_BUFFER, verts.len() as i32, glow::STATIC_DRAW);
            check_error(gl);

            gl.vertex_attrib_pointer_f32(0, 3, glow::FLOAT, false, 3, 0);
            check_error(gl);
            // gl::VertexAttribPointer(0, 3, gl::FLOAT, gl::FALSE, 3 * mem::size_of::<gl::types::GLfloat>() as gl::types::GLsizei, ptr::null());
            gl.enable_vertex_attrib_array(0); //gl::EnableVertexAttribArray(0);
            check_error(gl);

            // note that this is allowed, the call to gl::VertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
            // gl.bind_buffer(glow::ARRAY_BUFFER, Option::from(0));//gl::BindBuffer(gl::ARRAY_BUFFER, 0); //todo fix for webgl
            check_error(gl);

            // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
            // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
            // gl.bind_vertex_array(Option::from(0));//gl::BindVertexArray(0); //todo fix for webgl
            check_error(gl);

            let size = (verts.len() / 3) as i32;
            // uncomment this call to draw in wireframe polygons.
            // gl::PolygonMode(gl::FRONT_AND_BACK, gl::LINE);

            Model {
                vbo,
                vao,
                verts,
                mode,
                size,
            }
        }

        // Model::fromVerts(verts,gl::TRIANGLES)
    }
    #[allow(dead_code)]
    pub fn square(gl: &glow::Context) -> Model {
        let vertices: Vec<f32> = vec![
            -0.5, -0.5, 0.0, -0.5, 0.5, 0.0, 0.5, 0.5, 0.0, 0.5, -0.5, 0.0,
        ];
        Model::from_verts(vertices, gl, glow::TRIANGLE_FAN)
    }
    pub fn cube(gl: &glow::Context) -> Model {
        unsafe {
            let verts: Vec<f32> = CUBE.to_vec();
            let data = std::slice::from_raw_parts(
                verts.as_ptr() as *const u8,
                verts.len() * std::mem::size_of::<f32>(),
            );
            let vao = gl.create_vertex_array().unwrap();
            gl.bind_vertex_array(Some(vao));

            let vbo = gl.create_buffer().unwrap();
            gl.bind_buffer(glow::ARRAY_BUFFER, Some(vbo));

            gl.buffer_data_u8_slice(glow::ARRAY_BUFFER, data, glow::STATIC_DRAW);

            let f32_size: i32 = std::mem::size_of::<f32>() as i32;
            let stride = f32_size * 8;

            gl.vertex_attrib_pointer_f32(0, 3, glow::FLOAT, false, stride, 0);
            gl.enable_vertex_attrib_array(0);

            gl.vertex_attrib_pointer_f32(1, 3, glow::FLOAT, false, stride, 3 * f32_size);
            gl.enable_vertex_attrib_array(1);

            gl.vertex_attrib_pointer_f32(2, 2, glow::FLOAT, false, stride, 6 * f32_size);
            gl.enable_vertex_attrib_array(2);

            // gl.bind_buffer(glow::ARRAY_BUFFER, Option::from(0));//todo fix for webgl!
            // gl.bind_vertex_array(Option::from(0));
            let size = (verts.len() / 8) as i32;
            check_error(gl);
            // uncomment this call to draw in wireframe polygons.
            // gl::PolygonMode(gl::FRONT_AND_BACK, gl::LINE);

            Model {
                vbo,
                vao,
                verts,
                mode: glow::TRIANGLES,
                size,
            }
        }

        // Model::fromVerts(verts,gl::TRIANGLES)
    }

    pub fn draw_model(&self, gl: &glow::Context) {
        unsafe {
            gl.bind_vertex_array(Option::from(self.vao));
            gl.draw_arrays(self.mode, 0, self.size);
        }
    }
}

// impl Draw for Model{
//     pub fn draw(&self){
//         self.draw_model()
//     }
// }

// impl Drop for Model  {
//     fn drop(&mut self) {
//         unsafe{
//             gl::DeleteVertexArrays(1,&self.vao.unwrap());
//             gl::DeleteBuffers(1,&self.vbo.unwrap());
//         }
//     }
// }

static CUBE: [f32; 6 * 6 * 8] = [
    //vert              //normal            //UV
    -0.5, -0.5, -0.5, 0.0, 0.0, -1.0, 0.0, 0.0, //bottom
    0.5, -0.5, -0.5, 0.0, 0.0, -1.0, 1.0, 0.0, 0.5, 0.5, -0.5, 0.0, 0.0, -1.0, 1.0, 1.0, 0.5, 0.5,
    -0.5, 0.0, 0.0, -1.0, 1.0, 1.0, -0.5, 0.5, -0.5, 0.0, 0.0, -1.0, 0.0, 1.0, -0.5, -0.5, -0.5,
    0.0, 0.0, -1.0, 0.0, 0.0, -0.5, -0.5, 0.5, 0.0, 0.0, 1.0, 0.0, 0.0, //top
    0.5, -0.5, 0.5, 0.0, 0.0, 1.0, 1.0, 0.0, 0.5, 0.5, 0.5, 0.0, 0.0, 1.0, 1.0, 1.0, 0.5, 0.5, 0.5,
    0.0, 0.0, 1.0, 1.0, 1.0, -0.5, 0.5, 0.5, 0.0, 0.0, 1.0, 0.0, 1.0, -0.5, -0.5, 0.5, 0.0, 0.0,
    1.0, 0.0, 0.0, -0.5, 0.5, 0.5, -1.0, 0.0, 0.0, 1.0, 0.0, //negX
    -0.5, 0.5, -0.5, -1.0, 0.0, 0.0, 1.0, 1.0, -0.5, -0.5, -0.5, -1.0, 0.0, 0.0, 0.0, 1.0, -0.5,
    -0.5, -0.5, -1.0, 0.0, 0.0, 0.0, 1.0, -0.5, -0.5, 0.5, -1.0, 0.0, 0.0, 0.0, 0.0, -0.5, 0.5,
    0.5, -1.0, 0.0, 0.0, 1.0, 0.0, 0.5, 0.5, 0.5, 1.0, 0.0, 0.0, 1.0, 0.0, //posX
    0.5, 0.5, -0.5, 1.0, 0.0, 0.0, 1.0, 1.0, 0.5, -0.5, -0.5, 1.0, 0.0, 0.0, 0.0, 1.0, 0.5, -0.5,
    -0.5, 1.0, 0.0, 0.0, 0.0, 1.0, 0.5, -0.5, 0.5, 1.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.5, 0.5, 1.0,
    0.0, 0.0, 1.0, 0.0, -0.5, -0.5, -0.5, 0.0, -1.0, 0.0, 0.0, 1.0, //negY
    0.5, -0.5, -0.5, 0.0, -1.0, 0.0, 1.0, 1.0, 0.5, -0.5, 0.5, 0.0, -1.0, 0.0, 1.0, 0.0, 0.5, -0.5,
    0.5, 0.0, -1.0, 0.0, 1.0, 0.0, -0.5, -0.5, 0.5, 0.0, -1.0, 0.0, 0.0, 0.0, -0.5, -0.5, -0.5,
    0.0, -1.0, 0.0, 0.0, 1.0, -0.5, 0.5, -0.5, 0.0, 1.0, 0.0, 0.0, 1.0, //posY
    0.5, 0.5, -0.5, 0.0, 1.0, 0.0, 1.0, 1.0, 0.5, 0.5, 0.5, 0.0, 1.0, 0.0, 1.0, 0.0, 0.5, 0.5, 0.5,
    0.0, 1.0, 0.0, 1.0, 0.0, -0.5, 0.5, 0.5, 0.0, 1.0, 0.0, 0.0, 0.0, -0.5, 0.5, -0.5, 0.0, 1.0,
    0.0, 0.0, 1.0,
];
