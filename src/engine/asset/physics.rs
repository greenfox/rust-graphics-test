use rapier3d::prelude::*;

pub struct PhysicsEngine {
    pipeline: PhysicsPipeline,
    island_manager:IslandManager,
    gravity: Vector<Real>,
    integration_parameters: IntegrationParameters,
    broad_phase: BroadPhase,
    narrow_phase: NarrowPhase,
    pub bodies: RigidBodySet,
    pub colliders: ColliderSet,
    joints: JointSet,
    ccd_solver: CCDSolver,
}

impl PhysicsEngine {
    pub fn new() -> Self {
        // Here the gravity is -9.81 along the y axis.
        let pipeline = PhysicsPipeline::new();
        let island_manager = IslandManager::new();
        let gravity = vector![0.0, -9.81, 0.0];
        let integration_parameters = IntegrationParameters::default();
        let broad_phase = BroadPhase::new();
        let narrow_phase = NarrowPhase::new();
        let bodies = RigidBodySet::new();
        let colliders = ColliderSet::new();
        let joints = JointSet::new();
        let ccd_solver = CCDSolver::new();
        // We ignore physics hooks and contact events for now.

        let t = PhysicsEngine {
            pipeline,
            island_manager,
            gravity,
            integration_parameters,
            broad_phase,
            narrow_phase,
            bodies,
            colliders,
            joints,
            ccd_solver,
        };
        t
    }
    pub fn step(&mut self) {
        self.pipeline.step(
            &self.gravity,
            &self.integration_parameters,
            &mut self.island_manager,
            &mut self.broad_phase,
            &mut self.narrow_phase,
            &mut self.bodies,
            &mut self.colliders,
            &mut self.joints,
            &mut self.ccd_solver,
            &(),
            &(),
        )

    }
}
