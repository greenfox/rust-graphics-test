use glow::*;

#[allow(dead_code)]
pub struct Shader {
    #[allow(dead_code)]
    vert_source: String, //idk if I need keep these around?
    #[allow(dead_code)]
    frag_source: String,
    vert_compiled: glow::Shader,
    frag_compiled: glow::Shader,
    program: glow::Program,
}

use cgmath;

const VERTEX_SHADER_SOURCE: &str = include_str!("defaultShader/default.vert");
const FRAGMENT_SHADER_SOURCE: &str = include_str!("defaultShader/default.frag");

fn check_error(gl: &glow::Context) {
    let a = unsafe { gl.get_error() };
    if a != 0 {
        println!("gl error:{}", a);
    }
}
#[allow(dead_code)]
impl Shader {
    fn build_shader(gl: &glow::Context, vert: &str, frag: &str) -> Self {
        let check = |shader: glow::Shader| unsafe {
            println!(
                "{}:{}",
                if gl.get_shader_compile_status(shader) {
                    "shader success"
                } else {
                    "shader fail"
                },
                gl.get_shader_info_log(shader)
            )
        };
        // let (vert, frag) = (VERTEX_SHADER_SOURCE,FRAGMENT_SHADER_SOURCE);

        let (program, vert_compiled, frag_compiled) = unsafe {
            // vertex shader
            let vert_shader = gl.create_shader(glow::VERTEX_SHADER);
            let vert_shader = vert_shader.unwrap();
            gl.shader_source(vert_shader, vert);
            gl.compile_shader(vert_shader);
            check(vert_shader);
            // fragment shader
            let frag_shader = gl.create_shader(glow::FRAGMENT_SHADER);
            let frag_shader = frag_shader.unwrap();
            gl.shader_source(frag_shader, frag);
            gl.compile_shader(frag_shader);
            check(frag_shader);

            // link shaders
            let program = gl.create_program().unwrap();
            gl.attach_shader(program, vert_shader);
            gl.attach_shader(program, frag_shader);
            gl.link_program(program);

            let status = if gl.get_program_link_status(program) {
                "program success"
            } else {
                "program fail"
            };
            let log = gl.get_program_info_log(program);
            println!("{}:{}", status, log);

            // check for linking errors
            // gl.GetProgramiv(shaderProgram, gl.LINK_STATUS, &mut success);
            // if success != gl.TRUE as GLint {
            //     gl.GetProgramInfoLog(shaderProgram, 512, ptr::null_mut(), infoLog.as_mut_ptr() as *mut GLchar);
            //     println!("ERROR::SHADER::PROGRAM::COMPILATION_FAILED\n{}", str::from_utf8(&infoLog).unwrap());
            // }
            // gl.DetachShader(program,vertexShader);
            // gl.DetachShader(program,fragmentShader);
            // gl.DeleteShader(vertexShader);
            // gl.DeleteShader(fragmentShader);
            (program, vert_shader, frag_shader)
        };

        Shader {
            vert_source: String::from(VERTEX_SHADER_SOURCE),
            frag_source: String::from(FRAGMENT_SHADER_SOURCE),
            vert_compiled,
            frag_compiled,
            program,
        }
    }
    pub fn use_this(&self, gl: &glow::Context) {
        unsafe {
            gl.use_program(Option::from(self.program));
        }
    }
    pub fn new(gl: &glow::Context) -> Self {
        Shader::build_shader(gl, VERTEX_SHADER_SOURCE, FRAGMENT_SHADER_SOURCE)
    }
    pub fn new_limited_shader(gl: &glow::Context) -> Self {
        let (vertex_shader_source, fragment_shader_source) = (
            r#"#version 300 es

            layout (location = 0) in vec3 vertex;
            layout (location = 1) in vec3 normal;
            layout (location = 2) in vec2 uv;

            out vec2 UV;
            out vec2 vert;
            out vec3 color_frag;
            void main() {
                UV = uv;
                color_frag = normal;
                vert = vertex.xy;
                gl_Position = vec4(vertex, 1.0);
            }"#,
            r#"#version 300 es
            precision mediump float;

            in vec2 UV;
            in vec2 vert;
            in vec3 color_frag;
            out vec4 color;
            void main() {
                color = vec4(UV,0.0, 1.0);
            }"#,
        );
        Shader::build_shader(gl, vertex_shader_source, fragment_shader_source)
    }
    // pub fn shader_info(shader:u32) { //todo fix this
    //     unsafe {
    //         // status
    //         let mut status = glow::FALSE as glow::types::GLint;
    //         gl.GetShaderiv(shader, glow::COMPILE_STATUS, &mut status);
    //         // error handling
    //         if status != (gl.TRUE as gl.types::GLint) {
    //             let mut len = 0;
    //             gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &mut len);
    //             let mut buf: Vec<u8> = Vec::with_capacity((len + 1) as usize);
    //             buf.extend([b' '].iter().cycle().take(len as usize));
    //             let err: std::ffi::CString = std::ffi::CString::from_vec_unchecked(buf);
    //             gl.GetShaderInfoLog(shader, len, std::ptr::null_mut(),
    //                                 err.as_ptr() as *mut gl.types::GLchar);
    //             println!("failed to compile shader {}", err.to_str().unwrap());
    //         }
    //     }
    // }
    // fn get_error() {
    //     let a;
    //     unsafe { a = gl.GetError() }
    //     if a != gl.NO_ERROR {
    //         println!("Error code:{}", a);
    //     }
    // }
    #[allow(dead_code)]
    pub fn set_uniform_vec3f(&self, gl: &glow::Context, label: &str, input: cgmath::Vector3<f32>) {
        unsafe {
            let asdf = gl.get_uniform_location(self.program, label);
            let g = &asdf.unwrap();
            let a = Option::from(g);
            // let qwer = asdf.ok();
            gl.uniform_3_f32(a, input.x, input.y, input.z);
        }
    }
    pub fn set_uniform_matrix4f(
        &self,
        gl: &glow::Context,
        label: &str,
        input: &cgmath::Matrix4<f32>,
    ) {
        unsafe {
            let g = &gl.get_uniform_location(self.program, label).unwrap();
            let a = Option::from(g);
            let mat = [
                //todo: this is an ugly fat ass copy that needs to be fixed. But I couldn't figure out how to make cgmath's matrix typecast
                input.x.x, input.x.y, input.x.z, input.x.w, input.y.x, input.y.y, input.y.z,
                input.y.w, input.z.x, input.z.y, input.z.z, input.z.w, input.w.x, input.w.y,
                input.w.z, input.w.w,
            ];

            gl.uniform_matrix_4_f32_slice(a, false, &mat);
            check_error(gl);
        }
    }
    pub fn get_uniform_loc(
        &self,
        gl: &glow::Context,
        label: &str,
    ) -> Option<glow::UniformLocation> {
        unsafe { gl.get_uniform_location(self.program, label) }
    }
}

// impl Drop for Shader { //with glow, I might not actually NEED that...
//     fn drop(&mut self) {
//         unsafe {
//             gl.DetachShader(self.program, self.frag_compiled);
//             gl.DetachShader(self.program, self.vert_compiled);
//             gl.DeleteShader(self.vert_compiled);
//             gl.DeleteShader(self.frag_compiled);
//             gl.DeleteProgram(self.program);
//         }
//     }
// }
#[allow(dead_code)]
impl Shader {
    pub fn get_program(&self) -> glow::Program {
        self.program
    }
}
