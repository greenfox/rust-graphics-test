use cgmath::*;

// #[derive(Copy,Clone)]
pub struct Transform {
    pub rotation: Quaternion<f32>,
    pub scale: Vector3<f32>,
    pub translation: Vector3<f32>,
}

impl Transform {
    pub fn new() -> Self {
        let rotation = Quaternion::new(0.0, 0.0, 0.0, 1.0);
        let scale = Vector3::new(1.0, 1.0, 1.0);
        let translation = Vector3::new(0.0, 0.0, 0.0);
        Transform {
            rotation,
            scale,
            translation,
        }
    }
    pub fn to_mat(&self) -> Matrix4<f32> {
        let a = Matrix4::from_translation(self.translation)
            * Matrix4::from(self.rotation)
            * Matrix4::from_nonuniform_scale(self.scale.x, self.scale.y, self.scale.z);
        a
    }
    pub fn clone(&self) -> Self {
        Transform {
            rotation: self.rotation,
            scale: self.scale,
            translation: self.translation,
        }
    }
}
