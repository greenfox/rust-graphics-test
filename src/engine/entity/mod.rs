use rapier3d::prelude::*;
use super::asset;

pub struct BasicEntity {
    pub shader: asset::Shader,
    pub model: asset::Model,
    pub transform: asset::Transform,
}

impl BasicEntity {
    pub fn new(gl: &glow::Context) -> Self {
        let shader = asset::Shader::new(gl);
        let model = asset::Model::cube(gl);
        let transform = asset::Transform::new();
        Self {
            shader,
            model,
            transform,
        }
    }
    pub fn draw(&self, gl: &glow::Context, camera_mat4: &cgmath::Matrix4<f32>) {
        self.shader.use_this(gl);
        self.shader.set_uniform_matrix4f(
            &gl,
            "ModelViewProjection",
            &(camera_mat4 * self.transform.to_mat()),
        );
        self.model.draw_model(gl);
    }
    // pub fn clone(&self){}
}


pub struct PhysicsEntity {
    parent: BasicEntity,
    rigid_body_handle: RigidBodyHandle,
    #[allow(dead_code)]
    collider_handle: ColliderHandle,
}

impl PhysicsEntity {
    pub fn new(
        gl: &glow::Context,
        physics: &mut asset::PhysicsEngine,
        body_type: RigidBodyType,
        transform: asset::Transform,
    ) -> Self {
        let rigid_body = RigidBodyBuilder::new(body_type)
            .translation(
                vector![transform.translation.x,
                transform.translation.y,
                transform.translation.z]
            )
            .build();

        let rigid_body_handle = physics.bodies.insert(rigid_body);

        let collider = ColliderBuilder::cuboid(
            transform.scale.x * 0.5,
            transform.scale.y * 0.5,
            transform.scale.z * 0.5,
        )
        .build();
        let collider_handle =
            physics
                .colliders
                .insert_with_parent(collider, rigid_body_handle, &mut physics.bodies);

        let mut parent = BasicEntity::new(gl);
        parent.transform = transform;

        PhysicsEntity {
            parent,
            rigid_body_handle,
            collider_handle,
        }
    }
    pub fn new_dynamic(
        gl: &glow::Context,
        physics: &mut asset::PhysicsEngine,
        transform: asset::Transform,
    ) -> Self {
        Self::new(gl, physics, RigidBodyType::Dynamic, transform)
    }
    pub fn new_static(
        gl: &glow::Context,
        physics: &mut asset::PhysicsEngine,
        transform: asset::Transform,
    ) -> Self {
        Self::new(gl, physics, RigidBodyType::Static, transform)
    }
    pub fn update(&mut self, physics: &asset::PhysicsEngine) {
        let body = physics.bodies.get(self.rigid_body_handle).unwrap(); //todo handle fail
        let position = body.position();
        self.parent.transform.translation.x = position.translation.x;
        self.parent.transform.translation.y = position.translation.y;
        self.parent.transform.translation.z = position.translation.z;
        self.parent.transform.rotation.v.x = position.rotation[0];
        self.parent.transform.rotation.v.y = position.rotation[1];
        self.parent.transform.rotation.v.z = position.rotation[2];
        self.parent.transform.rotation.s = position.rotation[3];
    }
    pub fn draw(&self, gl: &glow::Context, camera_mat4: &cgmath::Matrix4<f32>) {
        self.parent.draw(gl, camera_mat4);
    }
}
