use super::asset;
use super::entity;

pub struct World {
    camera: asset::Camera,
    physics: asset::PhysicsEngine,
    entities: Vec<entity::PhysicsEntity>,
}

#[allow(dead_code)]
impl World {
    pub fn new(gl: &glow::Context, cube_count: i32) -> Self {
        let mut physics = asset::PhysicsEngine::new();
        let mut entities = Vec::new();

        //create camera
        let camera = {
            let mut camera =
                asset::Camera::perspective(&asset::Camera::new(), 80.0, 4.0 / 3.0, 0.001, 1000.0);
            camera.rotation = cgmath::Quaternion::from(cgmath::Euler {
                x: cgmath::Deg(10.0),
                y: cgmath::Deg(-45.0),
                z: cgmath::Deg(0.0),
            });
            camera.translation = cgmath::Vector3 {
                x: 30.0,
                y: 10.0,
                z: 30.0,
            };
            camera
        };

        //create floor
        {
            let floor = entity::PhysicsEntity::new_static(
                gl,
                &mut physics,
                asset::Transform {
                    rotation: cgmath::Quaternion::new(0.0, 0.0, 0.0, 1.0),
                    scale: cgmath::Vector3 {
                        x: 100.0,
                        y: 1.0,
                        z: 100.0,
                    },
                    translation: cgmath::Vector3::new(0.0, -10.0, 0.0),
                },
            );
            entities.push(floor);
        }
        //create boxes
        {
            let cube_spawner = asset::Transform {
                rotation: cgmath::Quaternion::new(0.0, 0.0, 0.0, 1.0),
                scale: cgmath::Vector3 {
                    x: 1.0,
                    y: 1.0,
                    z: 1.0,
                },
                translation: cgmath::Vector3::new(0.0, 10.0, 0.0),
            };
            let spin_rate = 5.0;
            let spacing = 0.051;
            let radius = 5.0;
            // let cube_count = 5000;
            for count in 0..cube_count {
                let mut p = cube_spawner.clone();
                p.translation.y += (count as f32) / spin_rate;
                let angle = (count as f32) * spacing * 6.2831853071795864769252;
                p.translation.x = angle.sin() * radius;
                p.translation.z = angle.cos() * radius;
                //.rotate_vector(cgmath::Vector2::unit_x());
                let cube = entity::PhysicsEntity::new_dynamic(gl, &mut physics, p);
                entities.push(cube);
            }
        }

        World {
            camera,
            physics,
            entities,
        }
    }
    pub fn update_aspect(&mut self, aspect: f32) {
        self.camera = asset::Camera::perspective(&self.camera, 80.0, aspect, 0.001, 1000.0);
    }
    fn step(&mut self) {
        self.physics.step();
    }
    fn draw(&mut self, gl: &glow::Context) {
        let camera_matrix = &self.camera.to_mat();
        for entity in self.entities.iter_mut() {
            entity.update(&self.physics);
            entity.draw(gl, camera_matrix);
        }
    }
    pub fn main(&mut self, gl: &glow::Context) {
        self.step();
        self.draw(gl);
    }
}
